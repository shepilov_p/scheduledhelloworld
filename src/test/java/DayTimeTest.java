import org.junit.Test;

import static org.junit.Assert.*;

public class DayTimeTest {

    @Test
    public void morningDayTimeTest() {
        assertEquals(DayTime.getByHour(7), DayTime.MORNING);
    }

    @Test
    public void middayDayTimeTest() {
        assertEquals(DayTime.getByHour(12), DayTime.DAY);
    }

    @Test
    public void eveningDayTimeTest() {
        assertEquals(DayTime.getByHour(22), DayTime.EVENING);
    }

    @Test
    public void nightDayTimeTest() {
        assertEquals(DayTime.getByHour(0), DayTime.NIGHT);
    }

    @Test
    public void topEdgeDayTimeTest() {
        assertEquals(DayTime.getByHour(23), DayTime.NIGHT);
    }

    @Test(expected = IllegalArgumentException.class)
    public void negativeHourTest() {
        DayTime.getByHour(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void outOfRangeHourTest() {
        DayTime.getByHour(99);
    }

}
