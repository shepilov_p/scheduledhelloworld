import org.junit.Test;

import java.io.OutputStream;
import java.io.PrintStream;
import java.time.LocalTime;

import static org.junit.Assert.assertEquals;

public class ConsoleHandlerTest {

    @Test
    public void printMessageTest() {
        PrintStream console = System.out;
        InterceptPrintStream out = new InterceptPrintStream(console);
        System.setOut(out);
        ConsoleHandler handler = new ConsoleHandler(new Messages("ru", "RU"));
        handler.setLocalTime(LocalTime.of(10, 0));
        handler.printMessage();
        assertEquals("Messages must match", out.getMessage(), "Добрый день, Мир!");
        System.setOut(console);
    }

    @Test
    public void printMessageWithUnknownSystemLocaleTest() {
        String language = System.getProperty(Messages.LANGUAGE_PROP_NAME);
        String country = System.getProperty(Messages.COUNTRY_PROP_NAME);
        System.setProperty(Messages.LANGUAGE_PROP_NAME, "unknown");
        System.setProperty(Messages.COUNTRY_PROP_NAME, "UNKNOWN");
        PrintStream console = System.out;
        InterceptPrintStream out = new InterceptPrintStream(console);
        System.setOut(out);
        ConsoleHandler handler = new ConsoleHandler(new Messages(null, null));
        handler.setLocalTime(LocalTime.of(10, 0));
        handler.printMessage();
        assertEquals("Messages must match", out.getMessage(), "Good day, World!");
        System.setOut(console);
        System.setProperty(Messages.LANGUAGE_PROP_NAME, language);
        System.setProperty(Messages.COUNTRY_PROP_NAME, country);
    }

    private class InterceptPrintStream extends PrintStream{

        private String message;

        public InterceptPrintStream(OutputStream out) {
            super(out);
        }

        @Override
        public void println(String x) {
            this.message = x;
        }

        public String getMessage() {
            return message;
        }
    }

}
