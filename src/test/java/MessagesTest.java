import org.junit.Test;

import static org.junit.Assert.*;

public class MessagesTest {

    @Test
    public void getFileNameFromArgsTest() {
        String filename = new Messages().getFilename("ua", "UA" );
        assertEquals("Filenames must match", filename, "messages_ua_UA.properties");
    }

    @Test
    public void getFileNameFromSystemLocaleTest() {
        String language = System.getProperty(Messages.LANGUAGE_PROP_NAME);
        String country = System.getProperty(Messages.COUNTRY_PROP_NAME);
        System.setProperty(Messages.LANGUAGE_PROP_NAME, "ru");
        System.setProperty(Messages.COUNTRY_PROP_NAME, "RU");
        String filename = new Messages().getFilename(null, null);
        assertEquals("Filenames must match", filename, "messages_ru_RU.properties");
        System.setProperty(Messages.LANGUAGE_PROP_NAME, language);
        System.setProperty(Messages.COUNTRY_PROP_NAME, country);
    }

    @Test
    public void getDefaultFileNameTest() {
        String language = System.getProperty(Messages.LANGUAGE_PROP_NAME);
        String country = System.getProperty(Messages.COUNTRY_PROP_NAME);
        System.getProperties().remove(Messages.LANGUAGE_PROP_NAME);
        System.getProperties().remove(Messages.COUNTRY_PROP_NAME);
        String filename = new Messages().getFilename("", "");
        assertEquals("Filenames must match", filename, "messages_en_US.properties");
        System.setProperty(Messages.LANGUAGE_PROP_NAME, language);
        System.setProperty(Messages.COUNTRY_PROP_NAME, country);
    }


    @Test
    public void morningUAMessageTest() {
        Messages messages = new Messages("ua", "UA");
        assertEquals("Messages must match", messages.getMessage(DayTime.MORNING), "Доброго ранку, Всесвіт!");
    }

}
