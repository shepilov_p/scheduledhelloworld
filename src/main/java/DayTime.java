public enum DayTime {

    NIGHT(6), MORNING(9), DAY(19), EVENING(23);

    private int finishHour;

    DayTime(int finishHour) {
        this.finishHour = finishHour;
    }

    static DayTime getByHour(int currentHour) {
        if (currentHour < 0)
            throw new IllegalArgumentException("Hour must be in 0 - 23 format");
        if (currentHour == 23) return NIGHT;
        for (DayTime dayTime : values()) {
            if (currentHour < dayTime.finishHour) return dayTime;
        }
        throw new IllegalArgumentException("Hour must be in 0 - 23 format");
    }

}
