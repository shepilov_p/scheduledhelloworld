public class Application {

    public static void main(String[] args) throws Exception {
        Messages messages = new Messages(System.getenv("language"), System.getenv("country"));
        ConsoleHandler consoleHandler = new ConsoleHandler(messages);
        consoleHandler.printMessage();
    }

}
