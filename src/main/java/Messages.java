import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Properties;

public class Messages {

    private Logger logger = LoggerFactory.getLogger(getClass());

    static String LANGUAGE_PROP_NAME = "user.language";

    static String COUNTRY_PROP_NAME = "user.country";

    private Properties messages = new Properties();

    Messages() {
    }

    public Messages(String language, String country) {
        loadMessages(getFilename(language, country));
    }

    public String getMessage(DayTime dayTime) {
        return messages.getProperty(dayTime.name().toLowerCase() + "_message");
    }

    public String getFilename(String language, String country) {
        String fileName = "messages";
        if (language != null && !language.isEmpty() && country != null && !country.isEmpty()) {
            logger.info("Language and country parameters are not defined. Default ones will be used");
        } else {
            logger.debug("Trying to get default system locale");
            Properties properties = System.getProperties();
            if (properties.containsKey(LANGUAGE_PROP_NAME) && properties.containsKey(COUNTRY_PROP_NAME)) {
                language = properties.getProperty(LANGUAGE_PROP_NAME);
                country = properties.getProperty(COUNTRY_PROP_NAME);
                logger.info("Language ({}) and country ({}) are defined from system properties", language, country);
            } else {
                language = "en";
                country = "US";
            }
        }
        fileName += "_" + language + "_" + country;
        fileName += ".properties";
        return fileName;
    }

    private void loadMessages(String fileName) {
        try {
            messages.load(new InputStreamReader(getClass().getResourceAsStream(fileName), Charset.forName("UTF-8")));
        } catch (Exception e) {
            logger.error("Failed to load messages from file {}", fileName);
            loadMessages(getFilename("en", "US"));
        }
    }

}
