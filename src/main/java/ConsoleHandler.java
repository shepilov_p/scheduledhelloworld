import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class ConsoleHandler {

    private Logger logger = LoggerFactory.getLogger(getClass());

    private Messages messages = new Messages();

    private LocalTime localTime;

    public ConsoleHandler() {
    }

    public ConsoleHandler(Messages messages) {
        this.messages = messages;
    }

    public void printMessage() {
        LocalTime time = getTime();
        logger.info("Printing message. Current time {}", time.format(DateTimeFormatter.ofPattern("HH:mm:ss")));
        int currentHour = time.getHour();
        String message = messages.getMessage(DayTime.getByHour(currentHour));
        System.out.println(message);
    }

    LocalTime getTime() {
        return localTime == null ? LocalTime.now() : localTime;
    }

    public void setLocalTime(LocalTime localTime) {
        this.localTime = localTime;
    }
}
